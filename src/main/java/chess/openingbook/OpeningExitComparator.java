package chess.openingbook;
//
import java.util.Comparator;
//
final class OpeningExitComparator implements Comparator<OpeningExit>
{
    @Override
    public int compare(OpeningExit openingExit1, OpeningExit openingExit2)
    {
        int result = Integer.compare(openingExit1.getAnalysisDepth(), openingExit2.getAnalysisDepth());
        //
        if (result != 0) return result;
        //
        return Integer.compare(openingExit1.getId(), openingExit2.getId());
    }
}

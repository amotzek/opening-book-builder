package chess.openingbook;
//
import chess.board.Board;
//
final class OpeningExit
{
    private final int id;
    private final Board board;
    private final int analysisDepth;
    //
    public OpeningExit(int id, Board board, int analysisDepth)
    {
        this.id = id;
        this.board = board;
        this.analysisDepth = analysisDepth;
    }
    //
    public int getId()
    {
        return id;
    }
    //
    public Board getBoard()
    {
        return board;
    }
    //
    public int getAnalysisDepth()
    {
        return analysisDepth;
    }
    //
    @Override
    public String toString() {
        return "OpeningExit{" +
                "id=" + id +
                ", analysisDepth=" + analysisDepth +
                '}';
    }
}

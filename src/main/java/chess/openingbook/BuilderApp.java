package chess.openingbook;
//
import chess.board.Board;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
//
public class BuilderApp
{
    private static final Logger logger = Logger.getLogger("BuilderApp");
    //
    public static void main(String[] args)
    {
        var endpoint = "https://chess-database-service-3hbx73e5ra-ey.a.run.app";
        //
        if (args.length > 0) endpoint = args[0];
        //
        var username = System.getenv("USERNAME");
        var password = System.getenv("PASSWORD");
        //
        logger.info(String.format("connect to %s with username %s", endpoint, username));
        logger.info(String.format("search with parameters %s", Arrays.toString(Board.STANDARD_PARAMETERS)));
        //
        var innerClient = new ServiceClient(endpoint, username, password);
        var client = new OptimizedServiceClient(innerClient);
        var boardSet = Collections.synchronizedSet(new HashSet<Board>());
        var boardQueue = new ArrayBlockingQueue<Board>(3);
        var pathQueue = new ArrayBlockingQueue<List<Board>>(3);
        var executor = Executors.newFixedThreadPool(2);
        executor.submit(new BoardAnalyzer(boardQueue, pathQueue, true));
        executor.submit(new AnalysisPutter(pathQueue, client, boardSet));
        //
        try
        {
            //noinspection InfiniteLoopStatement
            while (true)
            {
                var board = client.findTodoBoard();
                //
                if (!boardSet.contains(board))
                {
                    boardSet.add(board);
                    boardQueue.put(board);
                    //
                    logger.info(String.format("set contains %s boards", boardSet.size()));
                }
            }
        }
        catch (Exception e)
        {
            logger.log(Level.SEVERE, "unexpected exception", e);
        }
        //
        executor.shutdownNow();
    }
}

package chess.openingbook;
//
import java.util.Comparator;
//
final class QualifiedMoveComparator implements Comparator<QualifiedMove>
{
    public int compare(QualifiedMove qualifiedMove1, QualifiedMove qualifiedMove2)
    {
        int result = (qualifiedMove1.isFromOpening() ? 0 : 1) - (qualifiedMove2.isFromOpening() ? 0 : 1);
        //
        if (result != 0) return result;
        //
        result = Integer.compare(qualifiedMove2.getAnalysisDepth(), qualifiedMove1.getAnalysisDepth());
        //
        if (result != 0) return result;
        //
        result = Integer.compare(qualifiedMove2.getUsage(), qualifiedMove1.getUsage());
        //
        if (result != 0) return result;
        //
        return qualifiedMove1.getMove().toString().compareTo(qualifiedMove2.getMove().toString());
    }
}

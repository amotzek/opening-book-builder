package chess.openingbook;
//
import chess.board.Board;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
//
final class BoardAnalyzer implements Runnable
{
    private static final Logger logger = Logger.getLogger("BoardAnalyzer");
    private final BlockingQueue<Board> boards;
    private final BlockingQueue<List<Board>> paths;
    private final boolean infinite;
    //
    public BoardAnalyzer(BlockingQueue<Board> boards, BlockingQueue<List<Board>> paths, boolean infinite)
    {
        this.boards = boards;
        this.paths = paths;
        this.infinite = infinite;
    }
    //
    @Override
    public void run()
    {
        try
        {
            //noinspection LoopConditionNotUpdatedInsideLoop
            do
            {
                var board = boards.take();
                int depth = Integer.parseInt(board.getComment());
                var engine = new ChessEngine(board);
                var path = engine.findBestPath(depth);
                int pathSize = path.size();
                //
                if (pathSize <= 1)
                {
                    logger.warning(String.format("cannot find next move for %s", board));
                }
                else
                {
                    paths.put(path);
                }
            }
            while (infinite);
        }
        catch (Exception e)
        {
            logger.log(Level.SEVERE, "unexpected exception", e);
        }
    }
}

package chess.openingbook;
//
import chess.board.Board;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;
//
final class OptimizedServiceClient
{
    private static final int LAST_USE_DELAY = 14;
    private static final int REGULAR_DELAY = 5;
    private static final Logger logger = Logger.getLogger("ServiceClient");
    private final ServiceClient client;
    private Calendar lastUse;
    //
    public OptimizedServiceClient(ServiceClient client)
    {
        this.client = client;
    }
    //
    public Board findTodoBoard() throws InterruptedException
    {
        int noTodoCount = 0;
        //
        while (true)
        {
            optimizeContainerUse();
            var board = client.findTodoBoard();
            usedContainerNow();
            //
            if (board != null) return board;
            //
            noTodoCount++;
            //
            if (noTodoCount == 1)
            {
                logger.info("no todo");
            }
            else
            {
                logger.info(String.format("no todo for %s times", noTodoCount));
            }
            //
            throttle(noTodoCount);
        }
    }
    //
    public void putAnalysis(Board board, int analysisDepth) throws InterruptedException
    {
        int noTransmitCount = 0;
        optimizeContainerUse();
        //
        while (!client.putAnalysis(board, analysisDepth))
        {
            usedContainerNow();
            //
            if (++noTransmitCount > 1) logger.info(String.format("no transmit for %s times", noTransmitCount));
            //
            throttle(noTransmitCount);
            optimizeContainerUse();
        }
    }
    //
    public void clearNextMoves()
    {
        client.clearNextMoves();
    }
    //
    public List<QualifiedMove> queryNextMoves(Board board)
    {
        usedContainerNow();
        //
        return client.queryNextMoves(board);
    }
    //
    private void optimizeContainerUse() throws InterruptedException
    {
        if (wasContainerUsedRecently()) return;
        //
        var when = nextHalfHour();
        sleepUntil(when);
    }
    //
    //
    private synchronized boolean wasContainerUsedRecently()
    {
        if (lastUse == null) return false;
        //
        Calendar someTimeAgo = Calendar.getInstance();
        someTimeAgo.add(Calendar.MINUTE, -LAST_USE_DELAY);
        //
        return lastUse.after(someTimeAgo);
    }
    //
    private synchronized void usedContainerNow()
    {
        lastUse = Calendar.getInstance();
    }
    //
    private static void throttle(int count) throws InterruptedException
    {
        if (count <= 10)
        {
            sleepFor(30000L * fibonacci(count)); // maximal 28 Minuten
        }
        else
        {
            sleepUntil(nextFullHour());
        }
    }
    //
    private static long fibonacci(int n)
    {
        if (n <= 2) return 1L;
        //
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
    //
    private static Calendar nextFullHour()
    {
        var when = Calendar.getInstance();
        when.set(Calendar.MINUTE, 0);
        when.add(Calendar.HOUR, 1);
        //
        return when;
    }
    //
    private static Calendar nextHalfHour()
    {
        var when = Calendar.getInstance();
        int minute = when.get(Calendar.MINUTE);
        //
        if (minute > REGULAR_DELAY && minute < 30)
        {
            when.set(Calendar.MINUTE, 30);
        }
        else if (minute > 30 + REGULAR_DELAY)
        {
            when.set(Calendar.MINUTE, 0);
            when.add(Calendar.HOUR, 1);
        }
        //
        return when;
    }
    //
    private static void sleepUntil(Calendar when) throws InterruptedException
    {
        var now = Calendar.getInstance();
        long millis = when.getTimeInMillis() - now.getTimeInMillis();
        sleepFor(millis);
    }
    //
    private static void sleepFor(long millis) throws InterruptedException
    {
        if (millis > 15000L)
        {
            logger.info(String.format("sleep for %s seconds", millis / 1000L));
            Thread.sleep(millis);
        }
    }
}

package chess.openingbook;
//
import chess.board.Board;
import chess.board.Color;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import static chess.board.Board.STANDARD_PARAMETERS;
//
public class FixerApp
{
    private static final Logger logger = Logger.getLogger("FixerApp");
    //
    public static void main(String[] args)
    {
        var endpoint = "https://chess-database-service-3hbx73e5ra-ey.a.run.app";
        //
        if (args.length > 0) endpoint = args[0];
        //
        var username = System.getenv("USERNAME");
        var password = System.getenv("PASSWORD");
        //
        logger.info(String.format("connect to %s with username %s", endpoint, username));
        logger.info(String.format("search with parameters %s", Arrays.toString(STANDARD_PARAMETERS)));
        //
        var innerClient = new ServiceClient(endpoint, username, password);
        var client = new OptimizedServiceClient(innerClient);
        var boards = new ArrayBlockingQueue<Board>(1);
        var paths = new SynchronousQueue<List<Board>>(true);
        var executor = Executors.newFixedThreadPool(1);
        executor.submit(new AnalysisPutter(paths, client, null));
        //
        try
        {
            while (true)
            {
                var counter = new AtomicInteger();
                var openingExits = new PriorityQueue<>(new OpeningExitComparator());
                queryBadOpeningExits(client, Color.BLACK, new Board(), counter, openingExits);
                queryBadOpeningExits(client, Color.WHITE, new Board(), counter, openingExits);
                client.clearNextMoves();
                //
                if (openingExits.isEmpty())
                {
                    logger.info("no bad opening exit found");
                    //
                    return;
                }
                //
                var openingExit = openingExits.peek();
                //
                logger.info(String.format("found %s bad opening exits", openingExits.size()));
                logger.info(String.format("try to improve %s", openingExit));
                //
                var board = openingExit.getBoard();
                int analysisDepth = openingExit.getAnalysisDepth();
                board.setComment(Integer.toString(analysisDepth));
                boards.put(board);
                //
                var analyzer = new BoardAnalyzer(boards, paths, false);
                analyzer.run();
            }
        }
        catch (InterruptedException e)
        {
            // pass
        }
        //
        executor.shutdownNow();
    }
    //
    private static Board queryBadOpeningExits(OptimizedServiceClient client, Color playerColor, Board board, AtomicInteger counter, PriorityQueue<OpeningExit> openingExits)
    {
        var color = board.getColor();
        var qualifiedMoves = client.queryNextMoves(board);
        //
        if (qualifiedMoves.isEmpty())
        {
            if (!color.equals(playerColor)) board = board.getBeforeBoard();
            //
            if (isStablePosition(board)) return board;
            //
            return null;
        }
        //
        var value = getBoardValue(board);
        var minValue = value;
        Board minBoard = null;
        //
        if (color.equals(playerColor))
        {
            int analysisDepth = 0;
            boolean hasOpeningMoves = false;
            // nur gute Züge
            for (var qualifiedMove : qualifiedMoves)
            {
                var nextBoard = qualifiedMove.getToBoard();
                //
                if (qualifiedMove.isFromOpening())
                {
                    hasOpeningMoves = true;
                    var endBoard = queryBadOpeningExits(client, playerColor, nextBoard, counter, openingExits);
                    //
                    if (endBoard != null)
                    {
                        var endValue = getBoardValue(endBoard);
                        //
                        if (endValue < minValue)
                        {
                            minValue = endValue;
                            minBoard = endBoard;
                        }
                    }
                }
                else
                {
                    if (!hasOpeningMoves && isGoodMove(qualifiedMove))
                    {
                        var endBoard = queryBadOpeningExits(client, playerColor, nextBoard, counter, openingExits);
                        //
                        if (endBoard != null)
                        {
                            var endValue = getBoardValue(endBoard);
                            //
                            if (endValue < minValue)
                            {
                                minValue = endValue;
                                minBoard = endBoard;
                                analysisDepth = qualifiedMove.getAnalysisDepth();
                            }
                        }
                    }
                    //
                    break;
                }
            }
            //
            if (minBoard != null && analysisDepth > 0 && value - minValue > STANDARD_PARAMETERS[8])
            {
                int id = counter.incrementAndGet();
                var openingExit = new OpeningExit(id, board, analysisDepth);
                openingExits.add(openingExit);
                //
                logger.info(String.format("found path %s that worsened evaluation for %s from %.0f to %.0f", toPath(minBoard, board), playerColor, value, minValue));
                logger.info(String.format("enqueued opening exit %s", openingExit));
            }
        }
        else
        {
            // alle Züge
            for (var qualifiedMove : qualifiedMoves)
            {
                var nextBoard = qualifiedMove.getToBoard();
                var endBoard = queryBadOpeningExits(client, playerColor, nextBoard, counter, openingExits);
                //
                if (endBoard != null)
                {
                    var endValue = getBoardValue(endBoard);
                    //
                    if (endValue < minValue)
                    {
                        minValue = endValue;
                        minBoard = endBoard;
                    }
                }
            }
        }
        //
        return minBoard;
    }
    //
    private static double getBoardValue(Board board)
    {
        return board.getStaticValue(getBoardDepth(board), STANDARD_PARAMETERS);
    }
    //
    private static int getBoardDepth(Board board)
    {
        int depth = 0;
        //
        while (board != null)
        {
            depth++;
            board = board.getBeforeBoard();
        }
        //
        return depth;
    }
    //
    private static boolean isGoodMove(QualifiedMove qualifiedMove)
    {
        return qualifiedMove.getAnalysisDepth() >= 7;
    }
    //
    private static boolean isStablePosition(Board board)
    {
        var move = board.getBeforeMove();
        //
        return !move.isCapture() && !move.isPromotion();
    }
    //
    private static String toPath(Board board, Board markBoard)
    {
        var move = board.getBeforeMove();
        //
        if (move == null) return "*";
        //
        if (board.equals(markBoard)) return toPath(board.getBeforeBoard(), null) + " # " + move;
        //
        return toPath(board.getBeforeBoard(), markBoard) + " " + move;
    }
}

package chess.openingbook;
//
import chess.board.Board;
import chess.search.ChessMiniMaxSearch;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Logger;
//
final class ChessEngine
{
    private static final long MAX_TRIES = 1500000L * 3600L * 24L * 7L;
    //
    private static final Logger logger = Logger.getLogger("ChessEngine");
    private static final DecimalFormat decimalFormat = new DecimalFormat("#0.00");
    //
    private final Board board;
    //
    public ChessEngine(Board board)
    {
        this.board = board;
    }
    //
    public List<Board> findBestPath(int depth)
    {
        long tries = 60000000L;
        List<Board> path;
        //
        do
        {
            logger.info(String.format("start search with %s tries", tries));
            var search = new ChessMiniMaxSearch(board, Board.STANDARD_PARAMETERS);
            long then = System.currentTimeMillis();
            path = search.findBestPath(tries);
            long now = System.currentTimeMillis();
            double branchingFactor = search.getAverageBranchingFactor();
            long leafCount = search.getLeafCount();
            long performance = (1000L * leafCount) / (1L + now - then);
            long transpositionCount = search.getTranspositionCount();
            var messageBuilder = new StringBuilder();
            messageBuilder.append(decimalFormat.format(branchingFactor));
            messageBuilder.append(' ');
            messageBuilder.append(performance);
            messageBuilder.append(' ');
            messageBuilder.append(transpositionCount);
            //
            for (var element : path)
            {
                var move = element.getBeforeMove();
                messageBuilder.append(" ");
                messageBuilder.append(move);
            }
            //
            logger.info(messageBuilder.toString());
            //
            int pathSize = path.size();
            //
            if ((pathSize - 1) > depth) break;
            //
            while ((pathSize - 1) <= depth)
            {
                tries *= (0.5d + branchingFactor);
                pathSize++;
            }
        }
        while (tries <= MAX_TRIES);
        //
        return path;
    }
}

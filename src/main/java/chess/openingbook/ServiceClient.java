package chess.openingbook;
//
import chess.board.Board;
import chess.move.Move;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
//
final class ServiceClient
{
    private static final Comparator<QualifiedMove> comparator = new QualifiedMoveComparator();
    private static final JSONArray emptyArray = new JSONArray();
    private static final Logger logger = Logger.getLogger("ServiceClient");
    private final String endpoint;
    private final String authorization;
    private final HashMap<Board, JSONArray> responseByBoard;
    //
    public ServiceClient(String endpoint, String  username, String password)
    {
        this.endpoint = endpoint;
        //
        String usernamePassword = String.format("%s:%s", username, password);
        byte[] bytes = usernamePassword.getBytes(StandardCharsets.UTF_8);
        String base64 = Base64.getEncoder().encodeToString(bytes);
        authorization = "Basic " + base64;
        responseByBoard = new HashMap<>();
    }
    //
    public synchronized Board findTodoBoard()
    {
        try
        {
            var connection = open(endpoint + "/todoBoard", "GET");
            var content = read(connection);
            var boardObject = new JSONObject(content);
            int depth = boardObject.getInt("analysisDepth");
            var lineArray = boardObject.getJSONArray("line");
            var board = new Board();
            //
            for (int i = 0; i < lineArray.length(); i++)
            {
                var moveObject = lineArray.getJSONObject(i);
                var moveLabel = moveObject.getString("moveLabel");
                board = applyMove(board, moveLabel);
            }
            //
            board.setComment(Integer.toString(depth));
            //
            return board;
        }
        catch (Exception e)
        {
            logger.log(Level.WARNING, "cannot find board", e);
        }
        //
        return null;
    }
    //
    private static Board applyMove(Board board, String moveLabel)
    {
        var moves = board.generateMoves();
        //
        while (!moves.isEmpty())
        {
            var move = moves.poll();
            //
            if (moveLabel.equals(move.toString())) return board.applyMove(move);
        }
        //
        throw new IllegalArgumentException(String.format("move %s is not valid for board %s", moveLabel, board));
    }
    //
    public synchronized boolean putAnalysis(Board board, int analysisDepth)
    {
        var moves = toLine(board);
        //
        if (moves.isEmpty()) throw new IllegalArgumentException("no moves");
        //
        var resource = new StringBuilder();
        resource.append(endpoint);
        resource.append("/analysis?");
        appendLine(moves, resource);
        resource.append("&depth=");
        resource.append(analysisDepth);
        //
        try
        {
            var connection = open(resource.toString(), "GET");
            checkResponseCode(connection);
            //
            return true;
        }
        catch (IOException e)
        {
            logger.log(Level.WARNING, "cannot transmit analysis", e);
        }
        //
        return false;
    }
    //
    public synchronized void clearNextMoves()
    {
        responseByBoard.clear();
    }
    //
    public synchronized List<QualifiedMove> queryNextMoves(Board board)
    {
        var qualifiedMoveArray = responseByBoard.computeIfAbsent(board, this::innerQueryNextMoves);
        int length = qualifiedMoveArray.length();
        //
        if (length > 0)
        {
            try
            {
                var qualifiedMoves = new ArrayList<QualifiedMove>(length);
                //
                for (int index = 0; index < length; index++)
                {
                    var qualifiedMoveObject = qualifiedMoveArray.getJSONObject(index);
                    var moveLabel = qualifiedMoveObject.getString("moveLabel");
                    var openingLabelArray = qualifiedMoveObject.getJSONArray("openingLabels");
                    var analysisDepth = qualifiedMoveObject.optInt("analysisDepth", 0);
                    var usage = qualifiedMoveObject.optInt("usage", 0);
                    var moves = board.generateMoves();
                    //
                    while (!moves.isEmpty())
                    {
                        var move = moves.poll();
                        //
                        if (move.toString().equals(moveLabel))
                        {
                            var qualifiedMove = new QualifiedMove(board, move, openingLabelArray.length() > 0, analysisDepth, usage);
                            qualifiedMoves.add(qualifiedMove);
                            //
                            break;
                        }
                    }
                }
                //
                qualifiedMoves.sort(comparator);
                //
                return qualifiedMoves;
            }
            catch (JSONException e)
            {
                logger.log(Level.WARNING, "cannot query next moves", e);
            }
        }
        //
        return Collections.emptyList();
    }
    //
    private JSONArray innerQueryNextMoves(Board board)
    {
        var resource = new StringBuilder();
        resource.append(endpoint);
        resource.append("/nextMoves?");
        appendLine(toLine(board), resource);
        //
        try
        {
            var connection = open(resource.toString(), "GET");
            var content = read(connection);
            //
            return new JSONArray(content);
        }
        catch (FileNotFoundException e)
        {
            // pass
        }
        catch (Exception e)
        {
            logger.log(Level.WARNING, "cannot query next moves", e);
        }
        //
        return emptyArray;
    }
    //
    private static void appendLine(LinkedList<Move> moves, StringBuilder resource)
    {
        resource.append("line=");
        //
        for (var move : moves)
        {
            resource.append(URLEncoder.encode(move.toString(), StandardCharsets.UTF_8));
            resource.append('+');
        }
        //
        resource.setLength(resource.length() - 1);
    }
    //
    private static LinkedList<Move> toLine(Board board)
    {
        var moves = new LinkedList<Move>();
        //
        while (board != null)
        {
            var beforeBoard = board.getBeforeBoard();
            var beforeMove = board.getBeforeMove();
            //
            if (beforeMove != null) moves.addFirst(beforeMove);
            //
            board = beforeBoard;
        }
        //
        return moves;
    }
    //
    @SuppressWarnings("SameParameterValue")
    private HttpURLConnection open(String spec, String method) throws IOException
    {
        var url = new URL(spec);
        //
        return open(url, method);
    }
    //
    private HttpURLConnection open(URL url, String method) throws IOException
    {
        var connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        connection.setRequestProperty("Authorization", authorization);
        connection.setDoInput(true);
        //
        return connection;
    }
    //
    private static String read(HttpURLConnection connection) throws IOException
    {
        checkResponseCode(connection);
        //
        try (InputStream stream = connection.getInputStream())
        {
            byte[] bytes = stream.readAllBytes();
            //
            return new String(bytes, StandardCharsets.UTF_8);
        }
    }
    //
    private static void checkResponseCode(HttpURLConnection connection) throws IOException
    {
        int code = connection.getResponseCode();
        //
        if (code == 404) throw new FileNotFoundException();
        //
        if (code > 299) throw new IOException("code " + code);
    }
}

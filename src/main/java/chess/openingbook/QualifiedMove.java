package chess.openingbook;
//
import chess.board.Board;
import chess.move.Move;
//
final class QualifiedMove
{
    private final Board board;
    private final Move move;
    private final boolean fromOpening;
    private final int analysisDepth;
    private final int usage;
    //
    public QualifiedMove(Board board, Move move, boolean fromOpening, int analysisDepth, int usage)
    {
        this.board = board;
        this.move = move;
        this.fromOpening = fromOpening;
        this.analysisDepth = analysisDepth;
        this.usage = usage;
    }
    //
    public Board getToBoard()
    {
        return board.applyMove(move);
    }
    //
    public Move getMove()
    {
        return move;
    }
    //
    public boolean isFromOpening()
    {
        return fromOpening;
    }
    //
    public int getAnalysisDepth()
    {
        return analysisDepth;
    }
    //
    public int getUsage()
    {
        return usage;
    }
    //
    @Override
    public boolean equals(Object o)
    {
        if (o instanceof QualifiedMove)
        {
            var that = (QualifiedMove) o;
            //
            return move.equals(that.move);
        }
        //
        return false;
    }
    //
    @Override
    public int hashCode()
    {
        return move.hashCode();
    }
    //
    @Override
    public String toString()
    {
        return "QualifiedMove{" +
                "move=" + move +
                (fromOpening ? ", fromOpening=true" : ", analysisDepth=" + analysisDepth) +
                '}';
    }
}

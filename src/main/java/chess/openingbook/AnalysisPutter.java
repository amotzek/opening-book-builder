package chess.openingbook;
//
import chess.board.Board;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
//
final class AnalysisPutter implements Runnable
{
    private final BlockingQueue<List<Board>> paths;
    private final OptimizedServiceClient client;
    private final Set<Board> boards;
    //
    public AnalysisPutter(BlockingQueue<List<Board>> paths, OptimizedServiceClient client, Set<Board> boards)
    {
        this.paths = paths;
        this.client = client;
        this.boards = boards;
    }
    //
    @Override
    public void run()
    {
        try
        {
            //noinspection InfiniteLoopStatement
            while (true)
            {
                var path = paths.take();
                int pathSize = path.size();
                var board = path.get(0);
                var nextBoard = path.get(1);
                client.putAnalysis(nextBoard, pathSize - 1);
                //
                if (boards != null) boards.remove(board);
            }
        }
        catch (InterruptedException e)
        {
            // pass
        }
    }
}
